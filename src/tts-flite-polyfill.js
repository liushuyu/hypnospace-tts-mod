const audioCtx = new AudioContext();
let audioSource = null;

function tts_is_speaking() {
    return !!audioSource;
}

function tts_stop() {
    if (audioSource) audioSource.stop();
}

function tts_text(text, lang, uri, vol, rate, pitch) {
    const wave = window['tts_s'](text, vol, rate, pitch);
    const instance = window['tts_i'];
    const num_samples = instance._get_wave_info_int(wave, 1);
    let buffer = audioCtx.createBuffer(
        instance._get_wave_info_int(wave, 2),
        num_samples,
        instance._get_wave_info_int(wave, 0)
    );
    const bufferData = buffer.getChannelData(0);
    let samples = new Int16Array(
        instance.HEAP8.buffer,
        instance._get_wave_samples(wave),
        num_samples
    );
    instance._free_cst_wave(wave);
    for (let index = 0; index < bufferData.length; index++) {
        bufferData[index] = samples[index] / 32767.0;
    }
    audioSource = audioCtx.createBufferSource();
    audioSource.buffer = buffer;
    audioSource.connect(audioCtx.destination);
    audioSource.onended = () => audioSource = null;
    audioSource.start();
}

window.Module().then((instance) => {
    const speak_text = instance.cwrap('speak_text', 'number', ['string','number','number','number']);
    instance._initialize();
    window['tts_i'] = instance;
    window['tts_s'] = speak_text;
});

// exports
window['tts_text'] = tts_text;
window['tts_stop'] = tts_stop;
window['tts_is_speaking'] = tts_is_speaking;
