const has_system_tts = window.speechSynthesis && window.speechSynthesis.getVoices().length > 0;
function fetchFallback() {
    return $.getScript('flite.js').then(() => $.getScript('tts-flite-polyfill.js'));
}
if (!has_system_tts) {
    console.warn('System TTS unavailable: using fallback TTS.');
    fetchFallback().then(() => {
        // pollute the C2 engine TTS module so that it will use our fallback solution
        cr.plugins_.UserMedia.prototype.exps.VoiceCount = (ret) => ret.set_int(1);
        cr.plugins_.UserMedia.prototype.cnds.IsSpeaking = window.tts_is_speaking;
        // TODO: ResumeSpeaking PauseSpeaking unimplemented
        cr.plugins_.UserMedia.prototype.acts.SpeakText = window.tts_text;
        cr.plugins_.UserMedia.prototype.acts.StopSpeaking = window.tts_stop;
        // Create new runtime using the c2canvas
        cr_createRuntime('c2canvas');
        window['nwgui'].Window.get().show();
    });
} else {
    // Create new runtime using the c2canvas
    cr_createRuntime('c2canvas');
    window['nwgui'].Window.get().show();
}
