#include <stdlib.h>
#include <math.h>
#include "cst_features.h"
#include "flite.h"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#else
#define EMSCRIPTEN_KEEPALIVE
#endif

extern cst_voice *register_cmu_us_slt(const char *voxdir);
static cst_voice *voice = NULL;

EMSCRIPTEN_KEEPALIVE
void initialize() {
  flite_init();
  if (!voice) {
    voice = register_cmu_us_slt(NULL);
  }
}

static float volume_conversion(float db) {
  float vol = powf(10.f, db / 20.f);
  if (vol > 1.f)
    return 1.f;
  if (vol < 0.f)
    return 0.f;
  return vol;
}

EMSCRIPTEN_KEEPALIVE
cst_wave *speak_text(const char *text, float volume, float rate, float pitch) {
  cst_utterance *u = NULL;
  cst_utterance *utt = NULL;
  cst_wave *w = NULL;

  if (!voice)
    initialize();
  u = new_utterance();
  utt_set_input_text(u, text);
  // adjust the parameters
  feat_set_float(u->features, "duration_stretch", 1.0 / rate);
  feat_set_float(u->features, "gain", volume_conversion(volume));
  feat_set_float(u->features, "f0_mean", pitch);

  if ((utt = flite_do_synth(u, voice, utt_synth)) == NULL) {
    return NULL;
  }

  w = copy_wave(utt_wave(utt));
  delete_utterance(u);
  return w;
}

EMSCRIPTEN_KEEPALIVE
int get_wave_info_int(const cst_wave *wave, const int info) {
  switch (info) {
  case 0:
    return wave->sample_rate;
  case 1:
    return wave->num_samples;
  case 2:
    return wave->num_channels;
  default:
    return -1;
  }
}

EMSCRIPTEN_KEEPALIVE
short *get_wave_samples(const cst_wave *wave) { return wave->samples; }

EMSCRIPTEN_KEEPALIVE
void free_cst_wave(cst_wave *wave) {
  if (wave)
    delete_wave(wave);
}

EMSCRIPTEN_KEEPALIVE
void destroy() {
  if (voice)
    delete_voice(voice);
}
