const fs = require('fs');
const jszip = require('jszip');
const process = require('process');

if (!process.argv[2]) {
    console.error('Please specify the path to the package.nw file!');
    process.exit(1);
}

const filename = process.argv[2];
const regex = /jQuery\(document\)\.ready\(function \(\)\s+{.+?}\);/ms;
const loader_script = fs.readFileSync('./dist/preamble.js', 'utf-8');
let subst = `jQuery(document).ready(function () {\n// Injected code\n${loader_script}\n\t\t});\n`;
const package_file = fs.readFileSync(filename);
const flite_js = fs.createReadStream('./dist/flite.js');
const flite_wasm = fs.createReadStream('./dist/flite.wasm');
const polyfill = fs.createReadStream('./dist/tts-flite-polyfill.js');

const patched_filename = `${filename}.new`;
const patched_file = fs.createWriteStream(patched_filename);
const timestamp = new Date(0);
const common_options = { date: timestamp };

function streamToString(stream) {
    const chunks = [];
    return new Promise((resolve, reject) => {
        stream.on('data', (chunk) => chunks.push(Buffer.from(chunk)));
        stream.on('error', (err) => reject(err));
        stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
    })
}

jszip.loadAsync(package_file).then((zip) => {
    console.log('Adding additional resources ...');
    zip.file('flite.js', flite_js, common_options);
    zip.file('flite.wasm', flite_wasm, common_options);
    zip.file('tts-flite-polyfill.js', polyfill, common_options);
    console.log('Patching index.html ...');
    streamToString(zip.file('index.html').nodeStream()).then((html) => {
        const new_content = html.replace(regex, subst);
        zip.file('index.html', new_content, common_options);
        console.log('Generating a new package file ...');
        zip.generateNodeStream({ compression: "DEFLATE" }).pipe(patched_file).on('finish', () => {
            console.log(`Done. New package file generated at ${patched_filename}`);
        });
    });
});
