const fs = require('fs');

const subst = 'ENVIRONMENT_IS_NODE=false;';
const regex = /ENVIRONMENT_IS_NODE=.+?;/m;
const loader_script = fs.readFileSync('./dist/flite.js', 'utf-8');
const patched = loader_script.replace(regex, subst);
fs.writeFileSync('./dist/flite.js', patched);

console.log('Flite loader script patched.');
