const fs = require('fs');

const regex = /jQuery\(document\)\.ready\(function \(\)\s+{.+?}\);/ms;
const loader_script = fs.readFileSync('./dist/preamble.js', 'utf-8');
const index = fs.readFileSync('./index.html', 'utf-8');
let subst = `jQuery(document).ready(function () {\n// Injected code\n${loader_script}\n\t\t});\n`;
fs.writeFileSync('./dist/index.html', index.replace(regex, subst));

console.log('Hypnospace Outlaw index shell page patched.');
