#!/bin/bash -e

TOPDIR="$(dirname "$0")"
TOPDIR="$(readlink -f "$TOPDIR")"
FLITEDIR="$TOPDIR"/flite
FLITELIBDIR="$FLITEDIR"/build/x86_64-linux-gnu/
DISTDIR="$TOPDIR"/dist

check_exe() {
    if ! command -v "$1"; then
        echo "ERROR: Can not find $1 in your system. Please install it."
        if [[ "$1" == 'emcc' ]]; then
          echo "The emcc compiler is part of the Emscripten SDK."
          echo "Please see https://emscripten.org/docs/getting_started/downloads.html ."
        fi
        exit 1
    fi
}

echo '[+] Checking for required programs ...'
for i in 'git' 'node' 'yarn' 'emcc'; do
    check_exe "$i"
done

echo '[+] Checking for a working emcc ...'
emcc --version
cat << EOF | emcc -c -o /tmp/test.wasm -x c -flto -O3 -
int main() {return 0;}
EOF
rm -f /tmp/test.wasm

if [ ! -f "$FLITEDIR"/Makefile ]; then
    echo '[+] Updating submodules ...'
    pushd "$TOPDIR"
    git submodule update --init --recursive
    popd
fi

echo '[+] Building Flite ...'
pushd "$FLITEDIR"
[ -f config/config ] && make clean
CFLAGS="-O3 -flto" LDFLAGS="-Wl,-u,ntohs -Wl,-u,htons -sTOTAL_MEMORY=1073741824" emconfigure ./configure
emmake make
popd

echo '[+] Building binding module ...'
echo 'CC src/js-flite.c'
mkdir -p "$DISTDIR"
emcc -O3 -flto -sTOTAL_MEMORY=1073741824 -sMODULARIZE -sEXPORTED_RUNTIME_METHODS=ccall,cwrap \
-Wl,-u,ntohs -Wl,-u,htons \
-I"$FLITEDIR/include" -L"$FLITELIBDIR/lib" "$TOPDIR"/src/js-flite.c \
-lflite_cmu_us_slt -lflite_usenglish -lflite_cmulex -lflite -lm \
-o "$DISTDIR"/flite.js

echo '[+] Compressing shim scripts ...'
yarn --cwd "$TOPDIR"
echo 'TERSER tts-flite-polyfill.js'
yarn --cwd "$TOPDIR" run -- terser -o "$DISTDIR"/tts-flite-polyfill.js -c -m --toplevel -- "$TOPDIR"/src/tts-flite-polyfill.js
echo 'TERSER preamble.js'
yarn --cwd "$TOPDIR" run -- terser -o "$DISTDIR"/preamble.js -c -m --toplevel -- "$TOPDIR"/src/preamble.js

echo '[+] Patching loader script for use with NW.js ...'
pushd "$TOPDIR"
node ./scripts/patch-loader.js
popd

echo "All done! See ${TOPDIR}/dist for built assets!"
